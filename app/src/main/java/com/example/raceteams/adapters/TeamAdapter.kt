package com.example.raceteams.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.raceteams.R

import com.example.raceteams.model.Raceteam
import com.example.raceteams.model.convertDate


class TeamAdapter(
        private val teams: Array<Raceteam>,
        private val context: Context,
        private val teamSelectionListener: TeamSelectionListener
) : RecyclerView.Adapter<TeamAdapter.TeamViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        //val teamView = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        //return TeamViewHolder(teamView)
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.activity_recycler_item, parent, false)
        return TeamViewHolder(view)
    }

    override fun getItemCount() = teams.size

    override fun getItemViewType(position: Int): Int {
        return R.layout.activity_recycler_item
    }


    override fun onBindViewHolder(vh: TeamViewHolder, position: Int) {
        vh.team.text = teams[position].name
        vh.date.text = convertDate(teams[position].start_date)
        vh.wins.text = teams[position].wins.toString()
        vh.category.text = teams[position].category
        vh.logo.setImageDrawable(context.getDrawable(teams[position].image))
        vh.itemView.setOnClickListener {
            teamSelectionListener.onTeamSelected(position)
        }
    }

    interface TeamSelectionListener {
        fun onTeamSelected(position: Int)
    }

    class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val team: TextView = view.findViewById(R.id.item_team)
        val date: TextView = view.findViewById(R.id.item_date)
        val wins: TextView = view.findViewById(R.id.item_wins)
        val logo: ImageView = view.findViewById(R.id.item_logo)
        val category: TextView = view.findViewById(R.id.item_category)
    }
}
