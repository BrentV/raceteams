package com.example.raceteams.model

import com.example.raceteams.R
import java.text.SimpleDateFormat
import java.util.*


fun getRaceteams(): Array<Raceteam> {
    val rt1 = Raceteam(1,"Scuderia Ferrari", GregorianCalendar(1950,Calendar.MAY,15) ,237,"Formula 1", R.drawable.ferrari)
    val rt2 = Raceteam(2,"Mercedes-AMG Petronas F1", GregorianCalendar(1954,Calendar.MARCH,20),115, "Formula 1", R.drawable.mercedes)
    val rt3 = Raceteam(3,"Mclaren",GregorianCalendar(1966,Calendar.APRIL,2),183, "Formula 1",R.drawable.mclaren)
    val rt4 = Raceteam(4, "Red Bull Racing", GregorianCalendar(2005,Calendar.JULY,6), 63, "Formula 1",R.drawable.redbull)
    val rt5 = Raceteam(5, "Aston Martin", GregorianCalendar(2021,Calendar.FEBRUARY,1), 0, "Formula 1",R.drawable.astonmartin)
    val rt6 = Raceteam(6, "Hyundai Motorsport", GregorianCalendar(1997,Calendar.JUNE,10), 17, "Rally",R.drawable.hyundai)
    val rt7 = Raceteam(7, "Mercedes-Benz EQ", GregorianCalendar(2018,Calendar.NOVEMBER,25), 1, "Formula E",R.drawable.merceq)
    val rt8 = Raceteam(8, "Audi Sport ABT", GregorianCalendar(2018,Calendar.OCTOBER,8), 12, "Formula E",R.drawable.audisport)
    val rt9 = Raceteam( 9, "Rebellion Racing", GregorianCalendar(2010,Calendar.MARCH,23), 50, "SportsCar Racing",R.drawable.rebellion)
    val rt10 = Raceteam(10, "Alfa Romeo", GregorianCalendar(1950,Calendar.FEBRUARY,21), 10, "Formula 1",R.drawable.alfaromeo)
    val rt11 = Raceteam(11, "Toyota Gazoo Racing", GregorianCalendar(2017,Calendar.JUNE,30), 17, "Rally",R.drawable.toyota)
    val rt12 = Raceteam(12, "Prema Racing", GregorianCalendar(1983,Calendar.APRIL,17), 33, "Formula 2",R.drawable.prema)
    val rt13 = Raceteam(13, "ART Grand Prix", GregorianCalendar(2005,Calendar.MARCH,13), 26, "Formula 2",R.drawable.art)
    val rt14 = Raceteam(14, "Team Redline", GregorianCalendar(2000,Calendar.JULY,12), 268, "Sim Racing",R.drawable.redline)
    val rt15 = Raceteam(15, "Pure Racing Team", GregorianCalendar(2013,Calendar.JANUARY,1), 153, "Sim Racing",R.drawable.pureracing)

    return arrayOf(rt1,rt2,rt3,rt4,rt5,rt6,rt7,rt8,rt9,rt10,rt11,rt12,rt13,rt14,rt15)
}

fun convertDate(date:GregorianCalendar):String{
    val formatter = SimpleDateFormat("dd/MM/yyyy",Locale.US)
    println(date.time)
    return formatter.format(date.time)
}
