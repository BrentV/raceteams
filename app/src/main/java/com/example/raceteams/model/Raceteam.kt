package com.example.raceteams.model

import android.graphics.Bitmap
import java.util.*

data class Raceteam(
    val id: Int,
    val name: String,
    val start_date: GregorianCalendar,
    val wins: Int,
    val category: String,
    val image: Int
)
