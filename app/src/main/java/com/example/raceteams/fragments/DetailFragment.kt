package com.example.raceteams.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.raceteams.R
import com.example.raceteams.model.Raceteam
import com.example.raceteams.model.convertDate
import com.example.raceteams.model.getRaceteams

class DetailFragment: Fragment() {
    private lateinit var logo: ImageView
    private lateinit var teamName: TextView
    private lateinit var startDate: TextView
    private lateinit var wins: TextView
    private lateinit var category: TextView
    private var index: Int = 0


    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_main, container, false)

        logo = view.findViewById(R.id.imageLogo)
        teamName = view.findViewById(R.id.name)
        startDate = view.findViewById(R.id.date)
        wins = view.findViewById(R.id.wins)
        category = view.findViewById(R.id.category)

        updateFields()
        return view
    }


    private fun updateFields() {
        val teamList: Array<Raceteam> = getRaceteams()

        logo.setImageResource(teamList[index].image)
        teamName.text = teamList[index].name
        startDate.text = convertDate(teamList[index].start_date)
        wins.text = teamList[index].wins.toString()
        category.text = teamList[index].category
    }


    fun setTeamIndex(teamIndex: Int) {
        index = teamIndex
        updateFields()
    }

}