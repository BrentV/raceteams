package com.example.raceteams.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.raceteams.R
import com.example.raceteams.adapters.TeamAdapter
import com.example.raceteams.model.getRaceteams

class RecyclerFragment: Fragment() {
    private lateinit var listener: TeamAdapter.TeamSelectionListener


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TeamAdapter.TeamSelectionListener)
            listener = context
        else
            throw Exception()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_recycler, container, false)
        var rvFrag = view.findViewById<RecyclerView>(R.id.rvData)

        rvFrag.apply{
            layoutManager = LinearLayoutManager(activity)
            adapter = TeamAdapter(getRaceteams(),context,listener)
        }

        return view
    }

}