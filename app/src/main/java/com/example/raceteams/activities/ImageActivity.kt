package com.example.raceteams.activities

import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.example.raceteams.R
import com.example.raceteams.model.getRaceteams

class ImageActivity: AppCompatActivity() {
    private lateinit var image: ImageView
    private val teamList = getRaceteams()
    private var currentIndex: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        currentIndex = intent.getIntExtra("currentIndex",0)
        displayTeam()

    }

    private fun displayTeam(){
        image = findViewById(R.id.expLogo)
        image.setImageResource(teamList[currentIndex].image)
    }
}