package com.example.raceteams.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.raceteams.R
import com.example.raceteams.model.Raceteam
import com.example.raceteams.model.convertDate
import com.example.raceteams.model.getRaceteams

class MainActivity : AppCompatActivity() {
    private lateinit var image: ImageView
    private lateinit var name: TextView
    private lateinit var startDate:TextView
    private lateinit var wins:TextView
    private lateinit var category:TextView
    private lateinit var btnNext:Button
    private lateinit var btnPrevious:Button
    private val teamList: Array<Raceteam> = getRaceteams()
    private var currentIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currentIndex = intent.getIntExtra("currentIndex",0)
        initialiseViews()
        addEventHandlers()
        displayItem()
    }

    private fun initialiseViews(){
        image = findViewById(R.id.imageLogo)
        name=findViewById(R.id.name)
        startDate=findViewById(R.id.date)
        wins=findViewById(R.id.wins)
        category=findViewById(R.id.category)

    }

    private fun addEventHandlers() {
//        btnNext.setOnClickListener {
//            nextTeam()
//        }
//        btnPrevious.setOnClickListener {
//            previousTeam()
//        }
        image.setOnClickListener {
            val intent = Intent(this, ImageActivity::class.java)
            intent.putExtra("currentIndex", currentIndex)
            startActivity(intent)
        }
    }

    private fun displayItem() {
        image.setImageResource(teamList[currentIndex].image)
        name.text = teamList[currentIndex].name

        startDate.text = convertDate(teamList[currentIndex].start_date)

        wins.text=teamList[currentIndex].wins.toString()
        category.text=teamList[currentIndex].category

    }




}