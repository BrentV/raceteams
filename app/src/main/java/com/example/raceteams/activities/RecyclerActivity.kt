package com.example.raceteams.activities

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.raceteams.R
import com.example.raceteams.adapters.TeamAdapter
import com.example.raceteams.fragments.DetailFragment

class RecyclerActivity : AppCompatActivity(), TeamAdapter.TeamSelectionListener {
    //private lateinit var teams: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)
//        initialiseViews()
    }



//    private fun initialiseViews() {
//        teams = findViewById<RecyclerView>(R.id.rvData)
//                .apply {
//                    layoutManager =  LinearLayoutManager(this@RecyclerActivity)
//                    adapter = TeamAdapter(getRaceteams(),this@RecyclerActivity)
//                }
//    }



    override fun onTeamSelected(position: Int) {
//        val intent = Intent(this, MainActivity::class.java)
//        intent.putExtra("currentIndex",position)
//        startActivity(intent)
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("currentIndex", position)
            startActivity(intent)
        } else {
            val detailFrag = supportFragmentManager
                    .findFragmentById(R.id.detail_land_frag)
                    as DetailFragment
            detailFrag.setTeamIndex(position)
        }
    }
}
